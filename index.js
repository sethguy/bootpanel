import React, { useState } from 'react';
import { v4 } from 'uuid';

export const BootPanel = (props) => {

    const { children = [] } = props;
    const [panelId, setPanelId] = useState(v4());
    const childs = Array.isArray(children) ? children : [children]
    const [button] = childs.filter(ch => {
        return ch.type.name === 'BootPanelSummary'
    })

    const details = childs.filter(ch => {
        return ch.type.name !== 'BootPanelSummary'
    })

    const summary = [button]

    return (

        <React.Fragment>

            <div
                data-toggle="collapse"
                data-target={`#${panelId}`}
            >

                {summary.map(childNode => {
                    return React.cloneElement(childNode, {
                        ...childNode.props,
                        panelId,
                    })
                })}
            </div>
            <div
                id={panelId}
                className="collapse"
            >
                {details.map(childNode => {
                    return React.cloneElement(childNode, {
                        ...childNode.props,
                        panelId,
                    })
                })}
            </div>
        </React.Fragment>
    )
}


export const BootPanelSummary = (props) => {

    const { children, className = '', panelId = '' } = props;
    console.log("BootPanelSummary -> panelId", panelId)

    return (
        <div className={`boot-panel-summary ${className}`}>
            {children}
        </div>
    )
}

export const BootPanelDetails = (props) => {

    const { children, className = '', panelId = '' } = props;
    console.log("BootPanelDetails -> panelId", panelId)

    return (
        <div className={`boot-panel-details ${className}`}>
            {children}
        </div>
    )
}


export const BootPanelActions = (props) => {

    const { children, className = '', panelId = '' } = props;
    console.log("BootPanelActions -> panelId", panelId)

    return (
        <div className={`boot-panel-actions ${className}`}>
            {children}
        </div>
    )
}

